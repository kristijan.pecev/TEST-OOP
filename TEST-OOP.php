<?php

class Tezga
{
	public $id;
	public $plod;

	public function __construct($id, $name)
	{
		$this->id = $id;
		$this->name = $name;
	}
}

class Plodovi
{
	public $ime;
	public $cena;

	public function __construct($ime, $cena)
	{
		$this->ime = $ime;
		$this->cena = $cena;
	}
}

$portokal = new Plodovi('Portokal', 35);
$kompiri = new Plodovi('Kompiri', 240);
$orevi = new Plodovi('Orevi', 850);
$kivi = new Plodovi('Kivi', 670);
$piperki = new Plodovi('Piperki', 330);
$malini = new Plodovi('Malini', 555);

interface Cena
{
   public function total();
}
class kgPlodovi extends Plodovi implements Cena
{
   public $kilogrami;

   public function __construct($ime, $cena, $kilogrami)
   {
      parent::__construct($ime, $cena);
      $this->kilogrami = $kilogrami;
   }
   public function total()
   {
       $cena = $this->cena * $this->kilogrami;
       return $cena;
   }
}
class vrekaPlodovi extends Plodovi implements Cena
{
   public function __construct($ime, $cena)
   {
       parent::__construct($ime, $cena);
   }

   public function total()
   {
       return $this->cena;
   }
}

$tezga1 = new Plodovi('Portokal', 'Kompiri', 'Orevi');
$tezga2 = new Plodovi('Kivi', 'Piperki', 'Malini');

$array = ["Portokal"=>"3", "Orevi"=>"1", "Kivi"="1"];

foreach ($array as $key => $value) 
{
	echo $this->cena+=$value;
}

?>